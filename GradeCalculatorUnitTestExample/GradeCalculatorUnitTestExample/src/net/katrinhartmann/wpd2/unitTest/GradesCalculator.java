package net.katrinhartmann.wpd2.unitTest;

/**
 * The Grades of a student for a module
 * assessed by coursework and exam.
 */

public class GradesCalculator {
    private int courseworkMarks;
    private int examMarks;
    private String name;

    public GradesCalculator(String name) {
        this.name = name;
        this.courseworkMarks = 0;
        this.examMarks = 0;
    }

    public GradesCalculator(String name, int courseworkMarks, int examMarks) {
        this(name);
        this.courseworkMarks = courseworkMarks;
        this.examMarks = examMarks;
    }

    /*Checks that the argument m is in the range between 0 and 100 inclusive.
    * If not, an IllegalArgumentException is thrown.
    */
    void checkMarkInRange(int m) {
        if (m < 0 || m > 100) {
            throw new IllegalArgumentException("Mark is not between 0 and 100");
        }
    }

    public void setCourseworkMarks(int c) {
        checkMarkInRange(c);
        this.courseworkMarks = c;

    }

    public void setExamMarks(int e) {
        checkMarkInRange(e);
        this.examMarks = e;
    }

    /*Returns the average mark for this module*/
    public int getModuleAverage() {
        return (courseworkMarks + examMarks) / 2;
    }

    /**
     * Returns a String representing the grade classification.
     */
    public String getGrade() {
        String g = "not graded";
        final int m = getModuleAverage();
        if (m >= 70 && m <= 100) {
            g = "first class";
        } else if (m >= 60 && m < 70) {
            g = "2:1";
        } else if (m >= 50 && m < 60) {
            g = "2:2";
        } else if (m >= 40 && m < 50) {
            g = "third class";
        } else if (m > 0 && m< 40){
            g = "fail";
        }
        return g;
    }

    /*Returns a String showing the results.*/
    public String getResults() {
        StringBuffer out = new StringBuffer();
        out.append("******************************************\n");
        out.append("Results for student: " + name +"\n");
        out.append("******************************************\n");
        out.append("Final module mark: \t" + getModuleAverage() + "\n");
        out.append("of the above, coursework: " + courseworkMarks + "%\n");
        out.append("of the above, exam: " + examMarks + "%\n");
        out.append("Classification: " + getGrade() + "\n");
        out.append("******************************************\n");
        return out.toString();
    }

    public static void main(String[] args) {
        GradesCalculator peter = new GradesCalculator("Peter");
        peter.setCourseworkMarks(66);
        peter.setExamMarks(61);
        System.out.println(peter.getResults());
    }
}
