package net.katrinhartmann.wpd2.unitTest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

/**
 * Created by kat on 12/07/2017.
 */
public class GradesCalculatorTest {
    @Before
    public void setUp() throws Exception {
        //runs before each tests
        //used to set up any infrastructure that is used for the tests
        //such as set up connections to a database
    }

    @After
    public void tearDown() throws Exception {
        //runs after the test
        //often used to clear used resources,
        //such as close any open database connections
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetCourseworkMarksTooLowExceptionIsThrown() throws Exception {
        GradesCalculator gct = new GradesCalculator("Adam Testi");
        gct.setCourseworkMarks(-1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetCourseworkMarksTooHighExceptionIsThrown() throws Exception {
        GradesCalculator gct = new GradesCalculator("Adam Testi");
        gct.setCourseworkMarks(101);
    }

    @Test
    public void testGetGradeFirst() throws Exception {
        GradesCalculator gct = new GradesCalculator("Adam Testi", 70, 70);
        assertEquals("first class", gct.getGrade());
    }

    @Test
    public void testGetGradeTwoOne() throws Exception {
        GradesCalculator gct = new GradesCalculator("Adam Testi", 50, 70);
        assertEquals("2:1", gct.getGrade());
    }

    @Test
    public void testGetGradeFail() throws Exception {
        GradesCalculator gct = new GradesCalculator("Adam Testi", 25, 49);
        assertEquals("fail", gct.getGrade());
    }

    @Test
    public void testGetGradeUnassigned() throws Exception {
        GradesCalculator gct = new GradesCalculator("Adam Testi");
        assertEquals("not graded", gct.getGrade());
    }

    @Test
    public void testGetModuleAverage1() throws Exception {
        GradesCalculator gct = new GradesCalculator("Adam Testi", 0, 50);
        assertEquals(25, gct.getModuleAverage());
    }

    @Test
    public void testGetModuleAverage2() throws Exception {
        GradesCalculator gct = new GradesCalculator("Adam Testi", 50, 50);
        assertEquals(50, gct.getModuleAverage());
    }
}